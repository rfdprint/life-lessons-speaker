jQuery(document).ready(function( $ ) {

    $("#cat-selected-btn").click(function() {
        $("#StoryCatfrm").submit();
    }); 

    $('.ll-full-content-box-div').hide();
    
    
	$("#toggle-format-btn").click(function(){
       
        if($('.ll-excerpt-story-box-div').is(':visible')){
         $('.ll-excerpt-story-box-div').hide();
         
         $('.ll-full-content-box-div').show();
         $("#toggle-format-btn").text('Show Brief Excerpts');
        } else { 
            $('.ll-excerpt-story-box-div').show();
            $('.ll-full-content-box-div').hide();
            $("#toggle-format-btn").text('Show Full Stories');
            
        }
       
    });

});
