<?php
/**
 * Template Name: Testimonials
 * Description: A full width page template.
 *
 * @package First
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); ?>
<div class="full-width">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div id="testimonial-divs-blocks">
			<div id="testimonial-divs-block-1">
				<div id="tml-div1" class="testimonial-div-class">
					<?php echo do_shortcode("[RICH_REVIEWS_SHOW category='homepage-review#2']"); ?>		
				</div>
				<div id="tml-div2" class="testimonial-div-class">
					<?php echo do_shortcode("[RICH_REVIEWS_SHOW category='homepage-review#1']"); ?>
				</div>
				<div id="tml-div3" class="testimonial-div-class">
					<?php echo do_shortcode("[RICH_REVIEWS_SHOW category='review#3']"); ?>	
				</div>
				<div id="tml-div4" class="testimonial-div-class">
					<?php echo do_shortcode("[RICH_REVIEWS_SHOW category='homepage-review#3']"); ?>	
				</div>
			</div>
			<div id="testimonial-divs-block-2">
				<div id="tml-div5" class="testimonial-div-class"></div>
				<div id="tml-div6" class="testimonial-div-class"></div>
			</div>
			<div id="testimonial-divs-block-3">
				<div id="tml-div7" class="testimonial-div-class"></div>
				<div id="tml-div8" class="testimonial-div-class"></div>
			</div>	
			</div>	<!-- testemonials -->
			<!--echo do_shortcode("[siteorigin_widget class='SiteOrigin_Widget_PostCarousel_Widget'][/siteorigin_widget]"); ?>-->
			<div class="test-page">	
			<?php get_template_part( 'content', 'page' ); ?>	
			</div>	
		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>
