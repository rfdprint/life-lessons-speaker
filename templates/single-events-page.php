
<?php
/*
Template Name: Single Event
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); 


?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="entry-content">
				<div id="single-event-page-contant">

					<?php while ( have_posts() ) : the_post(); ?>
						<hr>
							<h1 class="single-event-page-title">
								<?php the_title();?>
							</h1>
						<hr>
							<?php the_content(); ?>
							
						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() ) :
								comments_template();
							endif;
						?>

					<?php endwhile; // end of the loop. ?>

				</div> <!-- #single-event-page-contant -->
			</div>		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>