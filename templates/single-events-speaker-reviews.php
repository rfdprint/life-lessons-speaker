<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if( get_field('speaker_photo') ):
?>
    <hr> <!-- divider -->
    <div id="single-event-page-columns">
        <div class="single-event-col1">
            <div class="single-event-speaker">
                <h2 class="event-page-columns-title">Featured Speaker</h2>
                <?php 
                $image = get_field('speaker_photo');

                if( !empty($image) ): ?>
                    <div class="speaker-photo-loop">
                    <?php 	
                        // vars
                        $url = $image['url'];
                        $title = $image['title'];
                        $alt = $image['alt'];
                        $caption = $image['caption'];

                        // thumbnail
                        $size = 'thumbnail';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];


                        if( $caption ): ?>
                        
                            <div class="wp-caption">

                            <?php endif; ?>

                            <a href="<?php echo $url; ?>" title="<?php echo $title; ?>">

                                <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                            </a>

                            <?php if( get_field('speaker_name') ): ?>
                            <p class="speaker-name-event-page";><strong>Speaker Name: <?php the_field('speaker_name'); ?></strong></p>
                            <?php endif; ?>

                            <?php if( get_field('speaker_quote') ): ?>
                            <p class="speaker-quote-event-page";><?php the_field('speaker_quote'); ?></p>
                            <?php endif; ?>

                            <?php if( $caption ): ?>

                                    <p class="wp-caption-text"><?php echo $caption; ?></p>

                                </div>

                            <?php endif; ?>
                        </div>
                <?php endif; ?>
            </div> <!-- single-event-speaker -->
        </div> <!-- single-event-col1 -->
        <div class="single-event-col2">
            <div class="ll-event-location">
                <h2 class="event-page-columns-title">Event Location</h2>
                    <?php if( get_post_meta(get_the_ID(), 'll_event_location', true)) : ?>  
                    <?php echo  get_post_meta(get_the_ID(), 'll_event_location', true);
                    else: ?>
                        <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/event-coming-soon.jpg' ?>">
                    <?php endif ?>
            </div>
        </div>
    </div> <!-- #single-event-page-columns -->
    <?php if (get_post_meta(get_the_ID(), 'll_event_interview_video', true)):?>
        <hr> <!-- divider -->
            <h1 class="single-event-page-title"> Student Interviews</h1>
        <hr> <!-- divider -->
        <div class="event-video-presentation">
        <?php echo do_shortcode(get_post_meta(get_the_ID(), 'll_event_interview_video', true));?>
        </div>
        <hr class="ll-event-interview-divider"> <!-- divider -->
    <?php endif; ?>

    <div class="single-event-page-review">
        <?php echo do_shortcode('[contact-form-7 id="929" title="Reviews"]');?>
        
        
    </div>
    <h1 class="single-reviews-title">Your Reviews</h1>
    <div class="single-event-review-display">
         <?php echo do_shortcode('[RICH_REVIEWS_SHOW category="post"]');?>
    </div>

<?php else:?>
    <style>
    
    .event-video-presentation {
        max-width: 100% !important;
        width: 100%;
    }
           
    </style>    
<?php endif; ?>

       

