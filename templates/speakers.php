<?php
/**
 * Template Name: Speakers Page
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); ?>
<div class="full-width">

	<div id="primary" class="content-area"> 
		<main id="main" class="site-main" role="main">
		<div class="speaker-page-content">
			<?php
				// set the "paged" parameter (use 'page' if the query is on a static front page)
				$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

				// the query
				$the_query = new WP_Query( 'tag=speaker&page=' . $paged ); 
			?>

			<?php if ( $the_query->have_posts() ) : ?>

				<?php
					// the loop************************************************
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					
						<div class="ll-speaker-box">
							<div class="ll-speaker-photo-div">				
                                    <?php if( get_field('speaker_photo') ): ?>
                                        <?php $image = get_field('speaker_photo'); ?>
                                        <img class="event-speaker-img ll-event-photo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                        <?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?>
                                     <p class="ll-speaker-name-div">

                                         <?php if( get_field('speaker_name') ): ?>
                                         <?php $speaker_name = the_field('speaker_name'); ?>
                                            <?php endif; ?>
                                        <?php echo $speaker_name; ?>
                                    </p>    	
                                    <?php else: ?>
                                                <?php
                                                echo '<img class="event-speaker-img ll-event-photo" src="/wp-content/uploads/2018/02/speaker-placeholder.jpg">';
                                            ?>
                                        <?php endif; ?>
                        </div> 
               
                        <div class="ll-speaker-quote-div">
                        
                            <?php if( get_field('speaker_quote') ): ?>
                            <?php
                                                echo '<img class="mic-micro-icon" src="/wp-content/uploads/2018/03/Speaker_Icon_72.png">';
                                            ?>
                                <p><?php the_field('speaker_quote'); ?></p>
                            <?php endif; ?>
                        </div>		
																								
						</div>
				<?php endwhile; //End loop************************************************ ?>

			<?php

				// next_posts_link() usage with max_num_pages
				next_posts_link( 'Older Entries', $the_query->max_num_pages );
				previous_posts_link( 'Newer Entries' );
			?>

			<?php 
				// clean up after the query and pagination
				wp_reset_postdata(); 
			?>

		    <?php else:  ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		    <?php endif; ?>

        </div>
		</main><!-- #main -->
	</div><!-- #primary -->
 </div>	
<?php get_footer(); ?>	
