<?php
/**
 * Template Name: Events Page
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); ?>

<div class="full-width">

	<div id="primary" class="content-area"> 
		<main id="main" class="site-main" role="main">
			<div id="event-page-content">
				<?php
					// set the "paged" parameter (use 'page' if the query is on a static front page)
					//$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

					// the query
					$the_query = new WP_Query( array( 'post_type' => 'event' ) );
				?>

				<?php if ( $the_query->have_posts() ) : ?>

					<?php
						// the loop************************************************
						while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						
							<div class="ll-event-box event-shortcode">
								<div class="ll-event-title"
										<a class="ll-event-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>	
										
									<div class="ll-event-speaker-photo-div">
										<?php if( get_field('speaker_photo') ): ?>
											<?php $image = get_field('speaker_photo'); ?>
											<img class="event-speaker-img ll-event-photo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											<?php //echo get_post(get_post_thumbnail_id())->post_excerpt; ?>	
										<?php else: ?>
												
												<img class="event-speaker-img ll-event-photo" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/speaker-placeholder.jpg'; ?>">
												
											<?php endif; ?>	
									
									</div>
									<div class="ll-event-speaker-name-div">
										<?php if( get_field('speaker_name') ): ?>
										<?php $speaker_name = the_field('speaker_name'); ?>
										<?php endif; ?>
										<?php echo $speaker_name; ?>
									</div>	
									<div class="ll-event-quote-div">
									
										<?php if( get_field('speaker_quote') ): ?>
											<p><?php the_field('speaker_quote'); ?></p>
										<?php endif; ?>
									</div>		
									<div class="ll-event-post-clip-thumbnail">  
											<?php echo '<h4 class="ll-event-post-clip-thumbnail-title" style="text-align: center; margin-bottom:0; color:#800000; margin-top: -15px;"> Introduction</h4>' ?>	
											<?php if( get_field('speaker_intro_video') ): ?>
											<?php $Intro_Video = get_field('speaker_intro_video'); ?>
											<?php echo do_shortcode($Intro_Video); ?>
											<?php elseif (get_the_post_thumbnail( $_post->ID, 'post-thumbnail' )):
												echo '<a href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
												echo get_the_post_thumbnail( $_post->ID, 'post-thumbnail' );
												echo '</a>';
											else:?>
												<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/event-coming-soon.jpg' ?>">
											<?php endif; ?>
									</div>
									<div class="event-social-share-icons">	
									<?php echo do_shortcode("[addtoany]"); ?>
									</div> 
									<div class="ll-event-buttons-div">
											<?php if( get_field('event_button_label_#1') ): ?>
											<?php $button_label = get_field('event_button_label_#1'); ?>
											<?php else: $button_label = 'Watch Full Video'; ?>
											<?php endif; ?>	
											
											<?php if( get_field('event-button_url_#1') ): ?>
											<?php $button_url = get_field('event-button_url_#1'); ?>
											<?php else: $button_url = get_post_permalink() ;?>
											<?php endif; ?>	
											
										<?php 
										echo '<a class="button ll-event-button button ll-button-1-label" href="'.$button_url.'">'.$button_label.'</a>'?>
										
										<?php if( get_field('event_button_label_#2') ): ?>
											<?php $button_label = get_field('event_button_label_#2'); ?>
											<?php else: $button_label = 'Become A Speaker'; ?>
											<?php endif; ?>	
											
											<?php if( get_field('event_button_url_#2') ): ?>
											<?php $button_url = get_field('event_button_url_#2'); ?>
											<?php else: $button_url = '/contact-us/' ;?>
											<?php endif; ?>	
											
										<?php 
										echo '<a class="button ll-event-button button" href="'.$button_url.'">'.$button_label.'</a>'?>
										
										
										<?php if( get_field('event_button_label_#3') ): ?>
											<?php $button_label = get_field('event_button_label_#3'); ?>
											<?php else: $button_label = 'Request An Event'; ?>
											<?php endif; ?>	
											
											<?php if( get_field('event_button_url_#3') ): ?>
											<?php $button_url = get_field('event_button_url_#3'); ?>
											<?php else: $button_url = '/contact-us/' ;?>
											<?php endif; ?>	
											
										<?php 
										echo '<a class="button ll-event-button button" href="'.$button_url.'">'.$button_label.'</a>'?>
									</div>
										
																	
												
							</div>
					<?php endwhile; //End loop************************************************ ?>

				<?php

					// next_posts_link() usage with max_num_pages
					next_posts_link( 'Older Entries', $the_query->max_num_pages );
					previous_posts_link( 'Newer Entries' );
				?>

				<?php 
					// clean up after the query and pagination
					wp_reset_postdata(); 
				?>

			<?php else:  ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

			</div><!-- #event-page-content -->
		</main><!-- #main -->
	</div><!-- #primary -->
 </div>	
<?php get_footer(); ?>	
