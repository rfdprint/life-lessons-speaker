<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div class="site-credit">
				<div class="site-copyright">
							<div>	
					<h5 class="terms-of-use-title">Terms of Use</h5>
					<div class="terms-of-use">		
					<p >Life Lessons not a counseling service nor does anyone associated with the company claim to be certified counselors. Please seek professional help if needed.  The use of this site and/or the suggestions from any person, speaker, user, administrator etc. is at your own risk.  Life Lessons is not responsible of any injury, death or lose of finances as a result of the use or suggestions from this website.</p> 
					<p> If you or anyone of concern has or is in a state of emergency call 911 immediately.</p>

					</div>	
				</div>
						<?php echo '© Copyright '. date(Y) . ' Life Lessons, LLC. | ';?>
						<?php echo '© Copyright '. date(Y) . ' RFDPrint.';?>
					</div>
					<?php printf( __( 'Content By <a href="%1$s">%2$s</a>', 'first' ), esc_url( __( 'https://lifelessonsspeaker.com/contact-us/', 'first' ) ), 'Life Lessons' ); ?>
					<span class="sep"> | </span>
					<?php printf( __( 'Website Design By <a href="%1$s">%2$s</a>', 'first' ), esc_url( __( 'https://rfdprint.com/', 'first' ) ), 'RFDPrint' ); ?>
					<div>
					<?php printf( __( 'Special thanks to <a href="%1$s">%2$s</a> for the responsive template.', 'first' ), esc_url( __( 'http://themehaus.net/', 'first' ) ), 'Themehaus' );?>
					</div>
</div>