<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="event-video-presentation">
        <h5 class="event-video-title">
            <?php echo get_post_meta(get_the_ID(), 'event_pre_video_title', true);?>
        </h5>       
    <?php   
    
    if (get_post_meta(get_the_ID(), 'll-event-presentation_video', true)):
        echo do_shortcode( get_post_meta(get_the_ID(), 'll-event-presentation_video', true));
        elseif(get_the_post_thumbnail( $_post->ID, 'post-thumbnail' )): 
            echo '<a href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
            echo get_the_post_thumbnail( $_post->ID, 'post-thumbnail' );
            echo '</a>';?>
            <h3 class="event-video-title-soon">Check back again later to watch this special video.</h3>
            <?php
             //Style for this is adjusted if the speaker and review section is missing (see single-events-speaker-reviews.php ) ?> 
            <style>
                .addtoany_content {
                display: none;
                }
            </style> 
            <?php  else:?>
            <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/event-coming-soon.jpg' ?>">
            <h3 class="event-video-title-soon">Check back again later to watch this special video.</h3> 
            <?php
             //Style for this is adjusted if the speaker and review section is missing (see single-events-speaker-reviews.php ) ?> 
            <style>
                .addtoany_content {
                display: none;
                }
            </style>
    <?php endif; ?>
</div>
<div class="event-excerpt">
        <?php echo '<p>'. the_excerpt() . '</p>' ;?>
</div>