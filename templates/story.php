<?php
/**
 * Template Name: Story
 * Template Post Type: story
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); ?>


<div class="full-width">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
			
                <div class="ll-story-div">
				<img class="mic-micro-icon" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/storyboard_Icon.png'; ?>">	
					<div class="story-text">   	
						<?php get_template_part( 'content', 'page' ); ?>
						<div class="story-nav">
						<div class="story-nav-left">
							<?php echo previous_post_link('%link','Previous Story',false); ?>
						</div>
						<div class="story-nav-right">
							<?php echo next_post_link('%link','Next Story',false); ?>
						</div>
					</div> <!-- end navigation -->
					</div>	
				</div>
				
				<div id="story-buttons">
					<div>  
						<a  class="story-share-btn" href= "<?php site_url()?>/contact-us/?contact-us=Share+My+Story">Share Your Story</a> 	
					</div>
					<div>
						<a class="story-become-speaker-btn" href="<?php site_url()?>/about-us/#become-a-speaker">Become a Speaker</a>    	
					</div>
				</div>		
			<?php
		       // Speakers Photos*************************************************************************
				// set the "paged" parameter (use 'page' if the query is on a static front page)
				$paged = ( get_query_var( 'speaker' ) ) ? get_query_var( 'speaker' ) : 1;

				// the query
				$the_query = new WP_Query( 'tag=speaker&page=' . $paged ); 
			?>
			<?php if ( $the_query->have_posts() ) : ?>

				<div class="event-notice-divider story-divider"> 	
				</div>
					<p class="story-speakers-title"> Life Lessons Speakers</p>
				<div class="event-notice-divider story-divider bottom"> 	
				</div>
					<?php
					
					// the loop************************************************
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					
						<div class="ll-speaker-box">
							<div class="ll-speaker-photo-div">

																
								<?php if( get_field('speaker_photo') ): ?>
								<?php $image = get_field('speaker_photo'); ?>

								<a href="<?php echo get_the_permalink() ?>"  >
										<img  class="event-speaker-img ll-event-photo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
									<p class="ll-speaker-name-div">

										<?php if( get_field('speaker_name') ): ?>
										<?php $speaker_name = the_field('speaker_name'); ?>
										<?php endif; ?>
									<?php echo $speaker_name; ?>
								</p>    	
								<?php else: ?>
											<?php
											echo '<img class="event-speaker-img ll-event-photo" src="/wp-content/uploads/2018/02/speaker-placeholder.jpg">';
										?>
								<?php endif; ?>
						
							</div> 	
																								
						</div>
					<?php endwhile; //End loop************************************************ ?>

			<?php

				// next_posts_link() usage with max_num_pages
				next_posts_link( 'Older Entries', $the_query->max_num_pages );
				previous_posts_link( 'Newer Entries' );
			?>

			<?php 
				// clean up after the query and pagination
				wp_reset_postdata(); 
			?>

		    <?php else:  ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		    <?php endif; ?>

    
			<?php	// End Speakers Photos*************************************************************************?>



				<?php
					/* If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;*/
				?>

			<?php endwhile; // end of the loop. ?>
   
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
