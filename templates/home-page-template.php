<?php
/**
 * Template Name: Home Page
 *
 */
 /**<?php if ( ! get_theme_mod( 'first_hide_navigation' ) ) : ?>*/
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
if ( is_front_page() ) :
	ll_home_page_header();
elseif ( is_404() ) :
	get_header( '404' );
else :
	get_header();
endif;
?>
<a href=\contact-us\><img id="home-page-header-img"  src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/life-lessons-home-page-header.jpg'?>"/></a>
	<div class="home-page-nav-bar">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<div class="menu-toggle"><?php _e( 'Menu', 'first' ); ?></div>
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				<?php if ( ! get_theme_mod( 'first_hide_search' ) ) : ?>
				<?php get_search_form(); ?>
				<?php endif; ?>
			</nav><!-- #site-navigation -->
	</div>
		
<div class="full-width">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>