<?php
/**
 * Template Name: Storyboard
 * Template Post Type: post, page, product
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
get_header(); ?>
<div class="full-width">

	<div id="primary" class="content-area"> 
		<main id="main" class="site-main" role="main">
		<div class="story-page-content">
			<div class="storyboard-divider"> 	
				<h1 class="storyboard-title">Storyboard</h1>
				<h4 class="storyboard-title subtitle">Life Stories from People with Experiences Like Yours</h4>
			</div>

				<h4 class="catergory-filter-title">Filter Stories by Category</h4>
				<?php $stories_catID = get_cat_ID('Stories') ?>
				<form id="StoryCatfrm" action="" method="POST"> <!--build the dynamic form-->
				<select id="storyselect" name="cat-selected">
					<option value="" <?php echo ($_POST['cat-selected'] == '' ) ? ' selected="selected" ' : ''; ?>>All Categories</option> 
					<?php $cat_args = array(
								'parent' 				   => $stories_catID,
								'type'                     => 'story',
								'orderby'                  => 'date',
								'order'                    => 'ASC',
								'hide_empty'               => true,
								'exclude'                  => ''
					);
					?> 
					<?php $categories = get_categories($cat_args); 
					foreach ($categories as $category) : 
						echo '<option value="'.$category->name.'"'; 
						echo ($_GET['cat-selected'] == ''.$category->name.'') ? ' selected="selected"' : ''; 
						echo '>'.$category->name.'</option>'; 
					endforeach; 
					?> 
				</select> 
				<button type="button" id="cat-selected-btn" >Submit Category</button>
				<button type="button" id="toggle-format-btn" >Show Full Stories</button>
				</form>
				<?php
					$filter_category = $_POST['cat-selected']; 
					echo '<h4 class="catergory-filter-title"> Filter Category: '. $filter_category.'</h4>';
				 ?>
				<div class="storyboard-options-divider">
				</div>

			<?php


			$filter_category= $_POST['cat-selected']; //get filter value 

			// set the "paged" parameter (use 'page' if the query is on a static front page)
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			// the query
				$args = array(
					'post_type'=>'story', // Your post type name
					'posts_per_page' => 50,
					'paged' => $paged,
					'category_name' => $filter_category
				);

			?>
			<div id="story-excerpt-loop">
				<?php
                    // the loop************************************************ 
                  
					$loop = new WP_Query( $args );
					if ( $loop->have_posts() ) :
						while ( $loop->have_posts() ) : $loop->the_post();?>
					
                    		<div class="ll-excerpt-story-box-div">

								<img class="story-hanger-2-img" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/story-hanger-mic-w-r.png'; ?>">															
							
								<div class="ll-story-excerpt-div">
									<div class="ll-story-excerpt">
									<div class="ll-story-excerpt-title"><?php the_title(); ?></div>
										<div class="excerpt">
											<?php the_excerpt(); 
											echo '<a class="story-excerpt-link" href="' . esc_url( get_the_permalink() ) . '"> Read more...</a>';
											?>
										</div>
									</div>	 
								</div> <!-- ll-excerpt-story-box-div-->	
							</div> <!-- ll-excerpt-story-box-div -->
							<div class="ll-full-content-box-div" >				
								<div class="ll-full-content-div">
								<img class="story-hanger-mic-img" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/story-hanger-mic-w.png'; ?>">		
								<img class="story-hanger-mic-r-img" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'images/story-hanger-mic-w-r.png'; ?>">														
									<div class="ll-story-content">
										<div class="ll-story-content-title"><?php the_title(); ?></div>
											<div class="ll-story-content-text"><?php the_content(); ?></div>
									</div> <!-- ll-story-content --> 
								</div> <!-- ll-story-content-div" -->   																		
							</div> 	<!-- ll-full-content-box-div -->	
							
						<?php endwhile; else : ?>
						 <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
			</div> <!-- #story-excerpt-loop -->	

			
			<div id="story-pagination">
			<?php
				$total_pages = $loop->max_num_pages;
			
				if ($total_pages > 1){
			
					$current_page = max(1, get_query_var('paged'));
			
					echo paginate_links(array(
						'base' => get_pagenum_link(1) . '%_%',
						'format' => 'page/%#%',
						'current' => $current_page,
						'total' => $total_pages,
						'prev_text'    => __('« Previous Board'),
						'next_text'    => __('Next Board »'),
					));
				}    
			wp_reset_postdata();
			?>
		  </div> <!-- #story-pagination -->	
        </div> <!-- story-page-content-->
		<?php get_template_part( 'content', 'page' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->
 </div><!-- full-width -->
<?php get_footer(); ?>	
