<?php 
/* Description: This plugin registers the 'product' post type.
 * Plugin Name:       Life Lessons Speaker
 * Plugin URI:        https://rfdprint.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.1
 * Author:            RFDPrint
 * Author URI:        https://rfdprint.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Life Lessons Storyboards
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}
/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'Life_Lessons_Storyboards', '1.0.1' );

/*-------------------------------------------------------------
 Name:      wpdocs_codex_story_init
 Purpose:   Register a custom posttype: story
-------------------------------------------------------------*/
function wpdocs_codex_story_init() {
    $labels = array(
        'name'                  => _x( 'Stories', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Story', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Storyboard', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Storyboard', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Storyboard', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Story', 'textdomain' ),
        'new_item'              => __( 'New Story', 'textdomain' ),
        'edit_item'             => __( 'Edit Story', 'textdomain' ),
        'view_item'             => __( 'View Story', 'textdomain' ),
        'all_items'             => __( 'All Stories', 'textdomain' ),
        'search_items'          => __( 'Search Stories', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Storiess:', 'textdomain' ),
        'not_found'             => __( 'No stories found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No stories found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Story Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Story archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into story', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this story', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter story list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Stories list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Stories list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'storyboards' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'taxonomies'         => array( 'category' ),
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('editor', 'thumbnail', 'excerpt', 'custom-fields'),
    );
 
    register_post_type( 'story', $args );
}
 
add_action( 'init', 'wpdocs_codex_story_init' );

/*-------------------------------------------------------------
 Name:      wpdocs_codex_event_init
 Purpose:   Register a custom posttype: event
-------------------------------------------------------------*/
function wpdocs_codex_event_init() {
    $labels = array(
        'name'                  => _x( 'Events', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Event', 'Post type singular name', 'textdomain' ),
        'name_admin_bar'        => _x( 'Event', 'Add New on Toolbar', 'textdomain' ),
        'menu_name'             => _x( 'Event', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Event', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Event', 'textdomain' ),
        'new_item'              => __( 'New Event', 'textdomain' ),
        'edit_item'             => __( 'Edit Event', 'textdomain' ),
        'view_item'             => __( 'View Event', 'textdomain' ),
        'all_items'             => __( 'All Events', 'textdomain' ),
        'search_items'          => __( 'Search Eventss', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Events:', 'textdomain' ),
        'not_found'             => __( 'No events found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No events found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Event Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set event image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove event image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as event image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Event archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into event', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this event', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter event list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Events list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Events list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'event' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'taxonomies'         => array('category', 'post_tag') ,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-pressthis',
        'supports'           => array('editor','excerpt','thumbnail', 'title'),
    );
 
    register_post_type( 'event', $args );
}
 
add_action( 'init', 'wpdocs_codex_event_init' );


function namespace_add_custom_types( $query ) {
  if( (is_category() || is_tag()) && $query->is_archive() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'story'
        ));
    }
    return $query;
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );

/*-------------------------------------------------------------
Name:      Multiple Functions
Purpose:   Set headers and content for columns on the admin screen
Post Type: "story"       
-------------------------------------------------------------*/

//Set header names
add_filter('manage_story_posts_columns', 'story_table_head');
function story_table_head ( $defaults ) {
    $defaults['story_excert']  = 'Excert';
    return $defaults;
}
add_action( 'manage_story_posts_custom_column', 'bs_event_table_content', 10, 2 );

//Sets content for columns
function bs_event_table_content( $column_name, $post_id ) {
    if ($column_name == 'story_excert') {
    $event_date = get_the_excerpt( $post_id, '_meta_story_excert', true );
      echo $event_date;
    }
}

/*-------------------------------------------------------------
Name:      Multiple Functions
Purpose:   Set headers and content for columns on the admin screen
Post Type: "event"       
-------------------------------------------------------------*/

//Set new columns postion before the 'Categories' column
    function your_columns_head($defaults) {  

        $new = array();
        $tags =  $defaults['speaker_name'];  // save the tags column
        unset( $defaults['speaker_name']);   // remove it from the columns list

        $tags =  $defaults['speaker_quote'];  // save the tags column
        unset( $defaults['speaker_quote']);   // remove it from the columns list

        foreach($defaults as $key=>$value) {
            if($key=='categories') {  // when we find the date column
            $new['speaker_name'] = $tags;  // put the tags column before it
            $new['speaker_quote'] = $tags;  // put the tags column before it 
            }    
            $new[$key]=$value;
        }  

        return $new;  
    } 
    add_filter('manage_event_posts_columns', 'your_columns_head');  

    //Set header names
    function event_table_head ( $defaults ) {
        $defaults['speaker_name']  = 'Speaker';
        $defaults['speaker_quote']  = 'Quote';
        
        return $defaults;
    }
    add_filter('manage_event_posts_columns', 'event_table_head');

    //Sets content for columns
    function single_event_admin_column_content( $column_name, $post_id ) {
        if ($column_name == 'speaker_name') {
        $speaker = get_post_meta( $post_id, 'speaker_name', true );
        echo $speaker;
        }
        if ($column_name == 'speaker_quote') {
            $speaker = get_post_meta( $post_id, 'speaker_quote', true );
            echo $speaker;
            }
    }
    add_action( 'manage_event_posts_custom_column', 'single_event_admin_column_content', 10, 2 );

    function example_insert_category() {
        wp_insert_term(
            'Stories',
            'category',
            array(
            'description'	=> 'This is the parent category for all of you story categoies (child categories). You will only be able to see and filter the child categories on the Storyboard page.',
            'slug' 		=> 'stories-category'
            )
        );
    }
    add_action( 'after_setup_theme', 'example_insert_category' );
        

// CHANGE EXCERPT LENGTH FOR DIFFERENT POST TYPES
/*-------------------------------------------------------------
Name:      set_custom_excerpt_length
Purpose:   Change excerpt length for different post types
Post Types: 'story' , 'products', 'testimonial'      
-------------------------------------------------------------*/
    function set_custom_excerpt_length($length) {
    global $post;
    if ($post->post_type == 'story')
    return 25;
    else if ($post->post_type == 'products')
    return 65;
    else if ($post->post_type == 'testimonial')
    return 75;
    else
    return 80;
    }

    add_filter('excerpt_length', 'set_custom_excerpt_length');


/*-------------------------------------------------------------
 Name:      Multiple
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   Designs Template: storyboard.php         
-------------------------------------------------------------*/    

    /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_storyboard_add_page_template ($templates) {
        $templates['storyboard.php'] = 'Storyboard';
        return $templates;
        }
    add_filter ('theme_page_templates', 'll_storyboard_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */
    function ll_storyboard_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_page_template', true ); if ('storyboard.php' == basename ($page_template ))
            $template = plugin_dir_path( __FILE__ ) . '/templates/storyboard.php';
        return $template;
        }
    add_filter ('page_template', 'll_storyboard_redirect_page_template');
 

/*-------------------------------------------------------------
 Name:      Multiple
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   Designs Template: story.php        
-------------------------------------------------------------*/    

    /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_story_add_page_template ($templates) {
        $templates['story.php'] = 'Story';
        return $templates;
        }
    add_filter ('theme_story_templates', 'll_story_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */
    function ll_story_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_post_template', true ); 
        
        if ('story.php' == basename ($page_template )) 
            $template = plugin_dir_path( __FILE__ ) . '/templates/story.php'; 
        return $template;
        }
    add_filter ('page_template', 'll_story_redirect_page_template');

    /* Auto assigns the custom template based on post type */
    function my_custom_template($single) {
        global $wp_query, $post;
    
        /* Checks for single template by post type */
        if ( $post->post_type == 'story' ) {
            if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/story.php' ) ) {
                return plugin_dir_path( __FILE__ ) . 'templates/story.php';
            }
        }
        return $single;
    
    }
    /* Filter the single_template with our custom function*/
    add_filter('single_template', 'my_custom_template');


/*-------------------------------------------------------------
 Name:      Multiple
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   Projects: single-events-page.php         
-------------------------------------------------------------*/

    /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_single_event_add_page_template ($templates) {
        $templates['single-events-page.php'] = 'Single Event';
        return $templates;
        }
    add_filter ('theme_story_templates', 'll_single_event_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */
    function ll_single_event_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_post_template', true ); 
        
        if ('single-events-page.php' == basename ($page_template )) 
            $template = plugin_dir_path( __FILE__ ) . '/templates/single-events-page.php'; 
        return $template;
        }
    add_filter ('event_template', 'll_single_event_redirect_page_template');

    /* Auto assigns the custom template based on post type */
    function ll_single_event_custom_template($single) {
        global $wp_query, $post;

        /* Checks for single template by post type */
        if ( $post->post_type == 'event' ) {
            if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/single-events-page.php' ) ) {
                return plugin_dir_path( __FILE__ ) . 'templates/single-events-page.php';
            }
        }
        return $single;

    }
    /* Filter the single_template with our custom function */
    add_filter('single_template', 'll_single_event_custom_template');


/*-------------------------------------------------------------
 Name:      ll_single_event_add_page_template
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   speakers.php       
-------------------------------------------------------------*/

    /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_speakers_add_page_template ($templates) {
        $templates['speakers.php'] = 'Speakers';
        return $templates;
        }
    add_filter ('theme_page_templates', 'll_speakers_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */
    function ll_speakers_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_page_template', true ); if ('speakers.php' == basename ($page_template ))
            $template = plugin_dir_path( __FILE__ ) . '/templates/speakers.php';
        return $template;
        }
    add_filter ('page_template', 'll_speakers_redirect_page_template');
    
/*-------------------------------------------------------------
 Name:      ll_single_event_add_page_template
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   events-page.php     
-------------------------------------------------------------*/

    /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_events_add_page_template ($templates) {
        $templates['events-page.php'] = 'Events Page';
        return $templates;
        }
    add_filter ('theme_page_templates', 'll_events_add_page_template');
    
    /* Redirects the wp from the theme folder to the plugin folder */
    function ll_events_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_page_template', true ); if ('events-page.php' == basename ($page_template ))
            $template = plugin_dir_path( __FILE__ ) . '/templates/events-page.php';
        return $template;
        }
    add_filter ('page_template', 'll_events_redirect_page_template');


/*-------------------------------------------------------------
 Name:      ll_single_event_add_page_template
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   content-event-loop-tag.php   
-------------------------------------------------------------*/

    /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_events_loop_add_page_template ($templates) {
        $templates['content-event-loop-tag.php'] = 'Events Loop';
        return $templates;
        }
    add_filter ('theme_page_templates', 'll_events_loop_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */    
    function ll_events_loop_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_page_template', true ); if ('content-event-loop-tag.php' == basename ($page_template ))
            $template = plugin_dir_path( __FILE__ ) . '/templates/content-event-loop-tag.php';
        return $template;
        }
    add_filter ('page_template', 'll_events_loop_redirect_page_template');


/*-------------------------------------------------------------
 Name:      Multiple
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   testimonials.php  
-------------------------------------------------------------*/

     /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_testimonials_add_page_template ($templates) {
        $templates['testimonials.php'] = 'Events Loop';
        return $templates;
        }
    add_filter ('theme_page_templates', 'll_testimonials_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */    
    function ll_testimonials_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_page_template', true ); if ('testimonials.php' == basename ($page_template ))
            $template = plugin_dir_path( __FILE__ ) . '/templates/testimonials.php';
        return $template;
        }
    add_filter ('page_template', 'll_testimonials_redirect_page_template');

/*-------------------------------------------------------------
 Name:      Multiple
 Purpose:   Finds the template 
            in the plugin folder and regesters it.
Template:   home-page-template.php 
-------------------------------------------------------------*/

     /*--Assigns a name to the plugin registered template 
    for the backend posttype page*/
    function ll_home_page_add_page_template ($templates) {
        $templates['home-page-template.php'] = 'Home Page';
        return $templates;
        }
    add_filter ('theme_page_templates', 'll_home_page_add_page_template');

    /* Redirects the wp from the theme folder to the plugin folder */    
    function ll_home_page_redirect_page_template ($template) {
        $post = get_post(); $page_template = get_post_meta( $post->ID, '_wp_page_template', true ); if ('home-page-template.php' == basename ($page_template ))
            $template = plugin_dir_path( __FILE__ ) . '/templates/home-page-template.php';
        return $template;
        }
    add_filter ('page_template', 'll_home_page_redirect_page_template');

/*-------------------------------------------------------------
 Name:      ll_home_page_header, display_custom_header
 Purpose:   Creates a custome hook ll_home_page_header(); loads function
            from display_custom_header for content
-------------------------------------------------------------*/

    function ll_home_page_header() {
        do_action('ll_home_page_header');
        
    }

    function display_custom_header() {
    include_once(plugin_dir_path( __FILE__ ) . '/templates/header-home-page.php');
    };
    add_action('ll_home_page_header', 'display_custom_header');

/*-------------------------------------------------------------
 Name:      ll_terms_footer, ll_speaker_footer
 Purpose:   Creates a custom hook ll_home_page_header(); loads function
            from display_custom_header for content
-------------------------------------------------------------*/

function ll_terms_footer() {
    do_action('ll_terms_footer');
    
}

function ll_speaker_footer() {
include_once(plugin_dir_path( __FILE__ ) . '/templates/terms-footer.php');
};
add_action('ll_terms_footer', 'll_speaker_footer');

/*-------------------------------------------------------------
 Name:      combine_title
 Purpose:   Adds the is to the end of the post title and slug
-------------------------------------------------------------*/

    function combine_title($title) {
    global $post;
    $type = get_post_type($post->ID);
    
    if ($type == 'story') {
        $title =  'Life Story # ' . $post->ID;
    }
    return $title;
    }

    add_filter ('title_save_pre','combine_title');

 
 /*-------------------------------------------------------------
 Name:      my_editor_content
 Purpose:   Adds defualt content to the text editor by post type
-------------------------------------------------------------*/  
function ll_event_video_presentation() {
    ob_start();
    include( plugin_dir_path( __FILE__ ) . '/templates/single-events-presentation.php');
    return ob_get_clean();   
 } 
 add_shortcode( 'll_event_video_presentation', 'll_event_video_presentation' ); 

 function ll_event_speaker_reviews() {
    ob_start();
    include( plugin_dir_path( __FILE__ ) . '/templates/single-events-speaker-reviews.php');
    return ob_get_clean();   
 } 
 add_shortcode( 'll_event_speaker_reviews', 'll_event_speaker_reviews' ); 
 
function my_editor_content( $content, $post ) {
 
    switch( $post->post_type ) {
        case 'event':
            $content =  
            '[ll_event_video_presentation]<br>[ll_event_speaker_reviews]';
        break;
    }
 
    return $content;
}
add_filter( 'default_content', 'my_editor_content', 10, 2 );

/* Post Filter****************************************************************
 function misha_filter_function(){
	$args = array(
		'orderby' => 'date', // we will sort posts by date
		'order'	=> $_POST['date'] // ASC или DESC
	);
 
	// for taxonomies / categories
	if( isset( $_POST['categoryfilter'] ) )
		$args['tax_query'] = array(
			array(
                'taxonomy' => 'category',
                'field' => 'id',
                'post-type' => 'story',
                'terms' => $_POST['categoryfilter']
                
			)
        );
        
	$query = new WP_Query( $args );
 
	if( $query->have_posts() ) :
		while( $query->have_posts() ): $query->the_post();
			echo '<h2>' . $query->post->post_title . '</h2>';
		endwhile;
		wp_reset_postdata();
	else :
		echo 'No posts found';
	endif;
 
	die();
}*/

/*-------------------------------------------------------------
 Name:      ll_current_events
 Purpose:   Creates a shortcode [ll_current_events] which loads
            the events post loop
-------------------------------------------------------------*/

function ll_current_events() {
    ob_start();
    include_once( plugin_dir_path( __FILE__ ) . '/templates/content-event-loop-tag.php');
    return ob_get_clean();   
 } 
 add_shortcode( 'll_current_events', 'll_current_events' );

 /*function themename_custom_header_setup() {
    $args = array(
        'default-image'      => plugin_dir_path( __FILE__ ) . '/images/logo_400_300_dpi-4.png',
        'default-text-color' => '000',
        'width'              => 1000,
        'height'             => 250,
        'flex-width'         => true,
        'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'themename_custom_header_setup' );*/

/*-------------------------------------------------------------
 Name:      rdas_my_assets
 Purpose:   Registers scripts and styles and enqueues them based
            on posttype.  This is to make sure that the needed
            scripts and styles are only loaded on the pages they
            are needed. 
-------------------------------------------------------------*/

function ll_my_assets() {
    wp_register_style ( 'life-lessons-style', plugins_url('/css/life-lessons-style.css', __FILE__) );
    wp_register_style ( 'storyboard-style', plugins_url('/css/storyboard-style.css', __FILE__) );
    wp_register_style ( 'story-style', plugins_url('/css/story-style.css', __FILE__) );
    wp_register_style ( 'events-style', plugins_url('/css/events-style.css', __FILE__) );
    wp_register_style ( 'testimonials', plugins_url('/css/testimonials.css', __FILE__) );
    wp_register_style ( 'speakers', plugins_url('/css/speakers.css', __FILE__) );
    wp_register_script( 'cat-filter', plugins_url('/js/cat-filter.js', __FILE__) );
    wp_register_script( 'toggle-content', plugins_url('/js/toggle-content.js', __FILE__) , array( 'jquery' ));
   
    wp_enqueue_style('life-lessons-style');

    $storyboard_page = get_page_by_title( 'Storyboard' );
    $storyboard_pageID = $storyboard_page->ID;
    if ( 'story' === get_post_type() || is_page($storyboard_pageID)){
        wp_enqueue_script('toggle-content');
        wp_enqueue_script('cat-filter' );
        wp_enqueue_style( 'storyboard-style' );
        wp_enqueue_style( 'story-style' );
    }

    $events_page = get_page_by_title( 'Events' );
    $speakers_page = get_page_by_title( 'Speakers' );

    $events_pageID = $events_page->ID;
    $speakers_pageID = $speakers_page->ID;

    if ( 'll-events' === get_post_type() 
    || is_page($events_pageID)
    || is_page($speakers_pageID) 
    ||'event' === get_post_type() 
    || is_front_page())
    
    {
        wp_enqueue_style( 'events-style' );
    }

    if ( is_page('testimonials')){
        wp_enqueue_style( 'testimonials' );
    }

    if ( is_page('speakers')){
        wp_enqueue_style( 'speakers' );
    }
        
}
add_action( 'wp_enqueue_scripts', 'll_my_assets' );

?>